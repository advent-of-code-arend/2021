#include "stdio.h"

int main(int argc, char** argv) {
    FILE* f = fopen("input.txt", "r");
    int x;
    int y = __INT16_MAX__;
    int n = 0;
    while (-1 != fscanf(f,"%d", &x)) {
        n += x > y;
        y = x;
    }
    fclose(f);
    FILE* g = fopen("output_a.txt", "w");
    fprintf(g, "%d\n", n);
    fclose(g);

    return 0;
}