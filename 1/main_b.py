with open('input.txt', 'r') as file:
    values = [int(x.strip()) for x in file.readlines()]

assert(len(values) == 2000)

a = values[0]
b = values[1]
c = values[2]
triplet = a + b + c
old_triplet = 999999999
cnt = 0
for i in range(1, len(values) - 2):
    if triplet > old_triplet:
        cnt += 1
    a = values[i]
    b = values[i + 1]
    c = values[i + 2]
    old_triplet = triplet
    triplet = a + b + c
if triplet > old_triplet:
    cnt += 1
with open('output_b.txt', 'w') as file:
    file.write(f"{cnt}\n")
