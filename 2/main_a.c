#include "stdio.h"
#include "stdlib.h"

typedef struct Position {
    int x;
    int y;
} Position;

Position *newPosition() {
    Position* p = (Position*) malloc(sizeof(Position));
    p->x = 0;
    p->y = 0;

    return p;
}

int toInteger(Position* p) {
    return p->x * p->y;
}

void executeCommand(Position *p, char command, int delta) {
    switch (command) {
        case 'f':
            p->x += delta;
            break;
        case 'u':
            p->y -= delta;
            break;
        case 'd':
            p->y += delta;
            break;
        default:
            fprintf(stderr, "Invalid value parsed, got %c\n", command);
            exit(EXIT_FAILURE);
    }
}


int main(int argc, char** argv) {
    Position *p = newPosition();
    FILE* f = fopen("input.txt", "r");
    char* buff = malloc(16);
    int delta;
    while (-1 != fscanf(f,"%s %d", buff, &delta)) {
        executeCommand(p, buff[0], delta);
    }
    fclose(f);
    FILE* g = fopen("output_a.txt", "w");
    fprintf(g, "%d\n", toInteger(p));
    fclose(g);
    free(p);
    return 0;
}