#include "stdio.h"
#include "stdlib.h"
#include "sys/stat.h"


int count_lines(char* filename) {
    FILE* f = fopen(filename, "r");
    int cnt = 0;
    char buff[16];
    while (-1 != fscanf(f, "%s", buff)) cnt++;
    fclose(f);

    return cnt;
}

int guess_buff_size(char* filename, int n_lines) {
    struct stat buff_stat;
    stat(filename, &buff_stat);
    return (buff_stat.st_size / n_lines) - 1;
}

void output_solution(int solution) {
    FILE* g = fopen("output_a.txt", "w");
    fprintf(g, "%d\n", solution);
    fprintf(stdout, "%d\n", solution);
    fclose(g);
}

int btod(int* digits, int length) {
    int factor = 1;
    int result = 0;
    for (int i = length - 1; i >= 0; i--) {
        result += factor * digits[i];
        factor *=2;
    }

    return result;
}

int* copy_invert_binary(int* digits, int length) {
    int* copy = malloc(length);
    for (int i = 0; i < length; i++ ) {
        copy[i] = !digits[i];
    }

    return copy;
}

void print_digits(int* digits, int length) {
    for (int i = 0; i < length; i++ ) {
        printf("%d", digits[i]);
    }
    printf("\n");
}


void calculate_solution(int* digits, int length, int n_lines) {
    int buff;
    int* normalized_digits = malloc(length * sizeof(int));
    for (int i = 0; i < length; i++) {
        buff = digits[i];
        printf("Considering digit %d with value %d\n", i, buff);
        normalized_digits[i] = (buff * 2 > n_lines);
    }
    free(digits);
    int* inverted_digits = copy_invert_binary(normalized_digits, length);
    print_digits(inverted_digits, length);
    print_digits(normalized_digits, length);
    int solution = btod(inverted_digits, length) * btod(normalized_digits, length);
    free(normalized_digits);
    free(inverted_digits);
    output_solution(solution);
}

int main(int argc, char** argv) {
    char* filename = argc > 1 ? argv[1] : "input.txt";
    int n_lines = count_lines(filename);
    int buff_size = guess_buff_size(filename, n_lines);
    int* digits = calloc(buff_size + 1, sizeof(int));
    char* digit_buff = malloc(buff_size + 1);
    FILE* f = fopen(filename, "r");
    int d;
    while (-1 != fscanf(f, "%s", digit_buff)) {
        for (int i = 0; i < buff_size; i++ ) {
            d = digit_buff[i] - 48;
            digits[i] += d;
        }
    }
    free(digit_buff);
    calculate_solution(digits, buff_size, n_lines);
    fclose(f);

    return 0;
}