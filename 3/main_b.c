#include "stdio.h"
#include "stdlib.h"
#include "sys/stat.h"

int n_rows, n_cols;
int** map;
FILE* f;
const char* filename = "input.txt";
int solution = -1;

typedef struct Keys {
    int* keys;
    int n;
    int bits;
} Keys;

typedef int rating_type;
#define OXYGEN 1
#define CARBON 2

int guess_buff_size();
int count_lines();
void setup();
int sum(int*, int);
void read_loop();
void cleanup();
void fail();
void solve();
int find_rating(rating_type);
void report_solution();
Keys filter_keys(Keys, int*, rating_type, int*);
Keys new_keys(int*, int, int*);
int common(Keys, rating_type);
int btod(int*, int);

int desired_bit(Keys keys, rating_type type) {
    int double_bits = keys.bits * 2;
    printf("Doubling the bits' value to: %d with n: %d\n", double_bits, keys.n);
    if (type == OXYGEN) {
        return double_bits >= keys.n;
    }
    return double_bits <= keys.n;
}

Keys filter_keys(Keys keys, int* col, rating_type type, int* bit) {
    int* nk = malloc(n_rows*sizeof(int));
    int new_key_index = 0;
    int filter_bit = desired_bit(keys, type);
    printf("Found desired bit to be of value: %d\n", filter_bit);
    *bit = filter_bit;
    for (int i = 0; i < keys.n; i++ ) {
        if (map[keys.keys[i]][*col] == filter_bit) {
            nk[new_key_index++] = keys.keys[i];
        }
    }

    free(keys.keys);
    ++(*col);

    return new_keys(nk, new_key_index, col);
}

Keys new_keys(int* key_ints, int n, int* col) {
    Keys keys = {
        .n = n,
        .keys = key_ints,
    };

    int c = 0;
    for (int i = 0; i < keys.n; i++) {
        c+=map[keys.keys[i]][*col];
    }

    keys.bits = c;

    return keys;
}

int find_rating(rating_type type) {
    int* initial_keys = malloc(n_rows*sizeof(int));
    int* bits = malloc(n_cols*sizeof(int));
    for (int i = 0; i < n_rows; i++) {
        initial_keys[i] = i;
    }
    int col = 0;
    Keys keys = new_keys(initial_keys, n_rows, &col);
    while (keys.n > 1 && col < n_cols) {
        printf("Checking col: %d\n", col);
        int bit;
        keys = filter_keys(keys, &col, type, &bit);
        printf("Found bit to be of value: %d\n", bit);
        bits[col - 1] = bit;
    }

    return btod(bits, col);
}


int btod(int* digits, int length) {
    int factor = 1;
    int result = 0;
    for (int i = length - 1; i >= 0; i--) {
        result += factor * digits[i];
        factor *=2;
    }

    return result;
}


void solve() {
    int oxygen = find_rating(OXYGEN);
    int carbon = find_rating(CARBON);
    solution = oxygen * carbon;
}

int guess_buff_size() {
    struct stat buff_stat;
    stat(filename, &buff_stat);
    n_cols = (buff_stat.st_size / n_rows) - 1;
    return n_cols;
}

int count_lines() {
    f = fopen(filename, "r");
    int cnt = 0;
    char buff[16];
    while (-1 != fscanf(f, "%s", buff)) cnt++;
    fclose(f);
    n_rows = cnt;

    return cnt;
}

void setup() {
    count_lines();
    guess_buff_size();
    map = malloc(n_rows*sizeof(int*));
    if (NULL == map) {
        fprintf(stderr, "Memory allocation failure for map\n");
        fail();
    }
    for (int i = 0; i < n_rows; i++) {
        void* memory = malloc((n_cols + 1)*sizeof(int));
        if (NULL == memory) {
            fprintf(stderr, "Memory allocation failure for map[%d]\n", i);
            fail();
        }
        map[i] = memory;
    }
    read_loop();
}

int sum(int* keys, int n) {
    int r = 0;
    for (int i = 0; i < n; i++) {
        r+= keys[i];
    }
    return r;
}

void read_loop() {
    f = fopen(filename, "r");
    char* input_buffer = malloc((n_cols + 1)*sizeof(int));
    int index = 0;
    while (-1 != fscanf(f, "%s", input_buffer)) {
        for (int i = 0; i < n_cols; i++) {
            map[index][i] = input_buffer[i] - 48;
        }
        index++;
    }
    free(input_buffer);
    fclose(f);
}

void report_solution() {
    fprintf(stdout, "%d\n", solution);
    FILE* g = fopen("output_b.txt", "w");
    fprintf(g, "%d\n", solution);
    fclose(g);
}

void cleanup() {
    if (NULL == f) fclose(f);
    for (int i = 0; i < n_rows; i++) {
        free(map[i]);
    }
    free(map);
    if (solution != -1) {
        report_solution();
    }
}

void fail() {
    cleanup();
    fprintf(stderr, "Successfully cleaned up memory on failure\n");
    exit(EXIT_FAILURE);
}

int main(int argc, char** argv) {
    setup();
    solve();
    cleanup();
}
